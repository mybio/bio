﻿function checkWeek() {
  var today = new Date();
  var dayOfWeek = today.getDay(); // 0 表示周日，1-6 表示周一到周六
  var weekNum = Math.ceil((today.getDate() - dayOfWeek + 1) / 7);
     if (weekNum % 2 === 0) {
      // 双周
var d=new Date()
var weekday=new Array(7)
weekday[0]=""
weekday[1]="物 数 化 英 体 语 数 英 物 语 生"
weekday[2]="英 生 音 物 英 英 化 数 数 语 语"
weekday[3]="语 数 化 物 体 数 生 化 语 英 物"
weekday[4]="数 化 劳 英 生 生 英 语 物 自 数"
weekday[5]="化 语 数 语 化 英 物 生 数 英 班"
weekday[6]=""
kb.firstChild.nodeValue =weekday[d.getDay()];
    } else {
      // 单周
var d=new Date()
var weekday=new Array(7)
weekday[0]=""
weekday[1]="物 数 化 英 体 语 数 英 物 语 生"
weekday[2]="英 生 音 物 英 英 化 数 数 语 语"
weekday[3]="语 数 化 物 体 数 生 化 语 英 物"
weekday[4]="数 化 劳 英 生 生 英 语 物 自 数"
weekday[5]="化 语 数 语 化 英 物 生 数 英 班"
weekday[6]=""
kb.firstChild.nodeValue =weekday[d.getDay()];
    }
}
onload=checkWeek();

 function getEndTime(myYear){
  var myEndTime = new Date(''+myYear+'/6/7 8:30:00');
  return myEndTime;
 }
 function countDown(){
  var mydate = new Date();
  var year = '2025';
  var EndTime = getEndTime(year);
  var NowTime = new Date();
  if( (EndTime.getTime() - NowTime.getTime() ) < 0){
   year = mydate.getFullYear() + 1;
   EndTime = getEndTime(year);
  }
  var t = EndTime.getTime() - NowTime.getTime();
  var d=Math.floor(t/1000/60/60/24);
  var h=Math.floor(t/1000/60/60%24);
  var m=Math.floor(t/1000/60%60);
  var s=Math.floor(t/1000%60);
  document.getElementById('day').innerText = d;
  document.getElementById('hour').innerText = h;
  document.getElementById('minute').innerText = m;
  document.getElementById('second').innerText = s;
 } 
 setInterval(countDown,1000);

function mrc() {
var butong_net =  new Array('让结局不留遗憾，让过程更加完美。(王旭家长寄语)','披星戴月走过的路，终将会繁花遍地。(陈华甜家长寄语)','问君何能尔，心远地自偏——汪静轩家长寄语(汪静轩家长寄语)','自信是你成功的基石，沉着稳重是你翱翔的翅膀。(段威博家长寄语)','一分耕耘，一分收获。梅花香自苦寒来。阳光总在风雨后。(方沅杰家长寄语)','逸一时，误一世，一言鼎真，坚定为赢(王晨希家长寄语)','无论风光与失意，把它留在过去，无论希望与梦想把它行动在今天，无论幸福与美好，把它展现在明天(梅君豪家长寄语)','愿你在成长的道路上一路欢笑，一路阳光，永远保持对美好事物的热爱和追求。(卢奕昕家长寄语)','君子豹变.其文蔚也.曾经我们也是祖国的花朵，共产主义接班人，.接下来就靠你们了！(吴逸杨家长寄语)','摒弃侥幸之念，必取百炼成钢；厚积分秒之功，始得一鸣惊人。(林盛钊家长寄语)','世上无难事无心人不就，科学有真谛有志者竟成。(陈刘馨梓家长寄语)','天道酬勤，精益求精，只争朝夕。(李欣雨家长寄语)','尽最大努力，留最小的遗憾，读书不是唯一的出路，但是读书是最好的出路(李博森家长寄语)','在学习的道路上，不要怕失败，因为每一次失败都是通向成功的必经之路。(赵鑫哲家长寄语)','过好每一天，吃好睡好学好玩好，越来越好。苏昊田家长寄语(苏昊田家长寄语)','三十年河东，三十年河西，莫欺少年穷！----炎帝·萧炎(曹博恩家长寄语)','心怀梦想，脚踏实地，未来可期。(金陆熙家长寄语)','全力以赴，用勤奋改变人生；永不言败，以执着成就未来。(施柯宇家长寄语)','勤学苦练，铸就辉煌；不负韶华，砥砺前行。(韦宇家长寄语)','成功要靠自己努力，哪怕只有万分之一的希望也不能放弃，坚信自己我能行！天道酬勤，宁静致远!(陈俊杰家长寄语)','亲爱的孩子，你要知道，你拥有无限的潜力，只要努力就能取得成功。在高中这段路上，你可能会遇到挫折和困难，但请记住，失败并不可怕，可怕的是你放弃。加油！(管唐寅家长寄语)','积跬步，累年月，则手熟尔；成心志，劳筋骨，不亦乐乎。(安皓铜家长寄语)','道阻且长，行则将至，行而不辍，未来可期(林嘉艺家长寄语)','时间记录着砥砺前进的足迹，岁月镌刻下拼搏奋斗的成就。(章子晗家长寄语)','吾志所向，一往无前;愈挫愈勇，再接再厉。(沈士杰家长寄语)','儿子，高中是你成为大人的一个转折点.是充满挑战与机遇。愿你以梦为马，不负韶华；以勤为帆，乘风破浪。学业有成，梦想照进现实。(徐永刚家长寄语)','儿子，高二是一个蜕变的机会，我们要珍惜这个阶段，用努力去成就一个更加美好的未来。无论路途有多么艰辛，相信自己，你一定可以成功！(钱嘉宇家长寄语)','向上的人生，最终还是要靠你自己。别辜负时光，去做自己人生的主角。愿你勇敢又努力，上进且坚定，所愿皆如愿。(钱颖琪家长寄语)','莫道浮云终蔽日，总有云开雾散时。不必踌躇过去，遗憾也是成长，会化作照亮前路的光。(朱梦洁家长寄语)','莫道浮云终蔽日，总有云开雾散时。不必踌躇过去，遗憾也是成长，会化作照亮前路的光。(赵雨欣家长寄语)','只有不断努力，你才能走向成功。不要因为一时的挫折而放弃，相信自己的潜力是无限的。(朱海涛家长寄语)','有平常心，就会有最佳的发挥，怀自信心，便能跨越每一个障碍。(杨宏成家长寄语)','正是礁石的阻挡，才使浪花显得美丽。(袁悠然家长寄语)','乾坤未定，你我皆是黑马，加油吧少年！(张睿乐家长寄语)',);
var butong_net2 = Math.floor(Math.random() * butong_net.length);
var hitokoto = document.getElementById("hitokoto");
hitokoto.firstChild.nodeValue = butong_net[butong_net2];
}
window.onload = mrc;